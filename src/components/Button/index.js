import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

export default function Button(props) {
    return (
        <button className='l-button' onClick={props.do}>
            {props.name}
        </button>
    );
}

Button.propTypes = {
    do: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
}