import React, { useRef, useState } from 'react';
import './style.scss';

import Button from 'components/Button/index';

export default function Canvas() {
    const [drawable, setDrawable] = useState(false);
    const [lastCoordinate, setLastCoordinate] = useState(null)
    const canvasRef = useRef();

    const drawLine = (e) => {
        if (drawable) {
            const canvasProperty = e.target.getBoundingClientRect();
            const posX = e.clientX - canvasProperty.left;
            const posY = e.clientY - canvasProperty.top;
    
            const context = canvasRef.current.getContext('2d');
            context.strokeStyle = "#000000";
            context.lineJoin = "round";
            context.lineWidth = 3;

            context.beginPath();
            if (lastCoordinate != null) {
                context.moveTo(lastCoordinate['posX'], lastCoordinate['posY']);
            } else {
                context.moveTo(posX, posY);
            }
            context.lineTo(posX, posY);
            context.closePath();
            context.stroke();
            setLastCoordinate({
                posX,
                posY,
            });
        }
    }

    const clearCanvas = () => {
        const canvas = canvasRef.current;
        const context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
    }

    return (
        <div id='l-canvas' >
            <div id='l-canvas__tools'>
                <Button name='reset' do={() => clearCanvas()} />
            </div>
            <canvas 
                id='c-canvas' 
                height='300'
                width='300'
                ref={canvasRef}
                onMouseMove={drawLine}
                onMouseDown={() => setDrawable(true)}
                onMouseUp={() => {
                        setDrawable(false)
                        setLastCoordinate(null);
                    }
                }
                onMouseLeave={() => {
                        setDrawable(false)
                        setLastCoordinate(null);
                    }
                }
            >
            </canvas>
        </div>
    );
}