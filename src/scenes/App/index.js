import React, { useState } from 'react';
import './style.scss';

import Canvas from './components/Canvas/index';

export default function App() {
  return (
    <section id='l-section' className='h-center'>
      <Canvas />
    </section>
  );
}
